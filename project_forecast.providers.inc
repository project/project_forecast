<?php
/**
 * @file
 * Project Forecast - Estimate completion dates for your project's tasks.
 *
 * This file contains an implementation of hook_value_provider_info(),
 * providing an 'hours' value type.
 *
 * Copyright 2007 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of hook_value_provider_info():
 * Return information about which fields can represent time need values,
 * and how to parse them into the number of hours required for this task.
 */
function project_forecast_value_provider_info() {
  $providers = array();

  if (module_exists('content') && module_exists('duration')) {
    foreach (content_fields() as $field) {
      $field_db_info = content_database_info($field);

      switch ($field['type']) {
        case 'duration':
          // Hours with standard 24/7 conversion factors.
          $provider = value_provider_content_provider_base($field);
          $provider['parse callback'] = 'project_forecast_parse_duration_hours';
          $providers['hours'][$field['field_name']] = $provider;

          // Hours with work week (8 hours a day, 5 days a week) conversion factors.
          $provider['title'] = t('!label - work week conversions', array(
            '!label' => value_provider_content_field_title($field)
          ));
          $provider['parse callback'] = 'project_forecast_parse_duration_hours_workweek';
          $providers['hours'][$field['field_name'] . '_workweek'] = $provider;
          break;
      }
    }
  }
  return $providers;
}

/**
 * Implementation of hook_value_provider_info_alter():
 * Duplicate all 'number' providers as 'hours' providers.
 */
function project_forecast_value_provider_info_alter(&$providers) {
  if (isset($providers['number'])) {
    foreach ($providers['number'] as $provider_name => $provider) {
      $providers['hours'][$provider_name] = $provider;
    }
  }
}

/**
 * Transform a duration field into the corresponding number of hours,
 * using standard 24/7 conversion factors.
 */
function project_forecast_parse_duration_hours($value_properties) {
  $seconds = isset($value_properties['approx_seconds'])
             ? $value_properties['approx_seconds']
             : 0;
  return $seconds / 3600;
}

/**
 * Transform a duration field into the corresponding number of hours,
 * using work week conversion factors.
 */
function project_forecast_parse_duration_hours_workweek($value_properties) {
  $iso_string = $value_properties['iso8601'];
  if (empty($iso_string)) {
    return 0;
  }
  $duration = duration_create($iso_string);
  if (isset($duration)) {
    $duration->set_conversion_factors(array(
      'days/months' => variable_get('project_forecast_days_per_month', 22),
      'days/weeks' => variable_get('project_forecast_days_per_week', 5),
      'hours/days' => variable_get('project_forecast_hours_per_day', 8),
    ));
    return $duration->to_single_metric('hours');
  }
  return 0;
}
