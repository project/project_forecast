<?php
/**
 * @file
 * Project Forecast - Estimate completion dates for your project's tasks.
 *
 * Copyright 2007, 2008 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * A handler to provide proper displays for time units (i.e. calendar weeks).
 */
class project_forecast_handler_field_time_unit extends views_handler_field {
  function init(&$view, &$data) {
    parent::init($view, $data);
    $this->additional_fields['completed'] = 'completed';
  }

  function render($values) {
    // When no estimated time is available (a.k.a. the field doesn't exist
    // in the database), return an empty string.
    if (!isset($values->{$this->aliases['completed']}) || !isset($values->{$this->field_alias})) {
      return '';
    }

    // Tell the user when a task could not be finished.
    if (empty($values->{$this->aliases['completed']})) {
      return t('not enough time');
    }

    // Display the date in a handsome fashion.
    $date = date_make_date($values->{$this->field_alias}, 'UTC', DATE_UNIX);
    if (is_object($date)) {
      return timebudget_format_unit($date);
    }
    return '';
  }

  function click_sort($order) {
    $order = strtolower($order);
    $opposite_order = ($order == 'desc') ? 'asc' : 'desc';
    $this->query->add_orderby($this->table, $this->field, $opposite_order, $this->aliases['completed']);
    $this->query->add_orderby($this->table, $this->field, $order, $this->field_alias);
  }
}
