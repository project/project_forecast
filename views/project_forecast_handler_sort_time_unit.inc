<?php
/**
 * @file
 * Project Forecast - Estimate completion dates for your project's tasks.
 *
 * Copyright 2007, 2008 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * A sort handler that properly takes non-completed targets into account.
 */
class project_forecast_handler_sort_time_unit extends views_handler_sort {
  function query() {
    $this->ensure_my_table();
    $order = strtolower($this->options['order']);

    $opposite_order = ($order == 'desc') ? 'asc' : 'desc';
    $this->query->add_orderby($this->table_alias, 'completed', $opposite_order);
    $this->query->add_orderby($this->table_alias, $this->real_field, $order);
  }
}
