<?php
/**
 * @file
 * Project Forecast - Estimate completion dates for your project's tasks.
 *
 * This file contains Views integration functionality.
 *
 * Copyright 2007 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of hook_views_handlers().
 */
function project_forecast_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'project_forecast') . '/views',
    ),
    'handlers' => array(
      // field handlers
      'project_forecast_handler_field_time_unit' => array(
        'parent' => 'views_handler_field',
      ),
      // sort handlers
      'project_forecast_handler_sort_time_unit' => array(
        'parent' => 'views_handler_sort',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 */
function project_forecast_views_data() {
  $data = array();

  // ----------------------------------------------------------------------
  // project_forecast_tasks table

  $data['project_forecast_tasks']['table']['group']  = t('Project forecast');

  $data['project_forecast_tasks']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['project_forecast_tasks']['unit'] = array(
    'title' => t('Estimated task completion date'),
    'field' => array(
      'handler' => 'project_forecast_handler_field_time_unit',
      'help' => t('The time when this task is estimated to be accomplished by all participating users. Will stay empty if the time budget has not been specified far enough to include this task in the estimated target date calculation.'),
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'title' => t('Has an estimated completion date'),
      'help' => t('Only display items where an estimated completion date has been determined. If this is not the case, then the user has not (yet) allocated enough time in the time budget to complete the task.'),
    ),
    'sort' => array(
      'handler' => 'project_forecast_handler_sort_time_unit',
      'help' => t('Sort by estimated completion date.'),
    ),
  );

  return $data;
}
