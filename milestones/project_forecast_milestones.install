<?php
/**
 * @file
 * Project forecast milestones - Provide milestone forecasts based on
 * the previously calculated task forecasts.
 *
 * Copyright 2008 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of hook_schema().
 */
function project_forecast_milestones_schema() {
  $schema['project_forecast_milestones_by_user'] = array(
    'description' => t('The table that contains estimated separate completion dates for each user in the "users with assigned cases" view, for all milestones that were found in the "milestones" view. If such a view has been specified (as it\'s optional) then this table remains empty.'),
    'fields' => array(
      'nid' => array(
        'description' => t('The {node}.nid node identifier of this milestone.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => t('The {user}.uid user identifier for the user to whom this cumulative completion date applies.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'unit' => array(
        'description' => t('The start of the time unit (i.e. calendar week) when the user milestone is estimated to be completed by the user. 0 if no estimated completion date could be calculated.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'completed' => array(
        'description' => t('Tells whether an estimated completion date could be calculated for this user milestone (which is the case as long as the user plans the time budget long enough in advance). 1 if the date could be calculated, or 0 if not.'),
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('nid', 'uid'),
  );
  $schema['project_forecast_milestones_total'] = array(
    'description' => t('The table that contains estimated total completion dates for all milestones that were found in the "milestones" view. If such a view has been specified (as it\'s optional) then this table remains empty.'),
    'fields' => array(
      'nid' => array(
        'description' => t('The {node}.nid node identifier of this milestone.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'unit' => array(
        'description' => t('The start of the time unit (i.e. calendar week) when the milestone is estimated to be completed. 0 if no estimated completion date could be calculated.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'completed' => array(
        'description' => t('Tells whether an estimated completion date could be calculated for this task (which is the case as long as all users plan their time budget long enough in advance). 1 if the date could be calculated, or 0 if not.'),
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('nid'),
  );
  return $schema;
}

/**
 * Implementation of hook_install().
 */
function project_forecast_milestones_install() {
  drupal_install_schema('project_forecast_milestones');
}

/**
 * Implementation of hook_uninstall().
 */
function project_forecast_milestones_uninstall() {
  drupal_uninstall_schema('project_forecast_milestones');

  foreach ($variables as $variable) {
    variable_del('project_forecast_milestone_field');
  }
}
