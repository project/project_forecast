<?php
/**
 * @file
 * Project forecast milestones - Provide milestone forecasts based on
 * the previously calculated task forecasts.
 *
 * This file contains Views integration functionality.
 *
 * Copyright 2007 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of hook_views_data().
 */
function project_forecast_milestones_views_data() {
  $data = array();

  // ----------------------------------------------------------------------
  // project_forecast_milestones table

  $data['project_forecast_milestones_total']['table']['group']  = t('Project forecast');

  $data['project_forecast_milestones_total']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['project_forecast_milestones_total']['unit'] = array(
    'title' => t('Estimated milestone completion date'),
    'field' => array(
      'handler' => 'project_forecast_handler_field_time_unit',
      'help' => t('The time when this milestone is estimated to be accomplished by all participating users. Will stay empty if the time budget has not been specified far enough to include all tasks for this milestone.'),
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'title' => t('Has an estimated completion date'),
      'help' => t('Only display items where an estimated completion date has been determined. If this is not the case, then the users responsible for this milestone have not (yet) allocated enough time in the time budget to complete all the tasks in there.'),
    ),
    'sort' => array(
      'handler' => 'project_forecast_handler_sort_time_unit',
      'help' => t('Sort by estimated completion date.'),
    ),
  );

  return $data;
}
