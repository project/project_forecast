<?php
/**
 * @file
 * Project Forecast - Estimate completion dates for your project's tasks.
 *
 * This file contains the "task filters" part of the administrative
 * user interface. For the largest part, this is a slightly modified copy
 * of certain functions in Views' admin.inc.
 *
 * Copyright 2007, 2008 by Earl Miles ("merlinofchaos", http://drupal.org/user/26979)
 * Copyright 2008 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

function project_forecast_admin_task_filters_open_add(&$form_state) {
  if (empty($form_state['storage']['phase'])) {
    return project_forecast_admin_task_filters_open_add_select($form_state);
  }
  else if ($form_state['storage']['phase'] == 'config-item') {
    return project_forecast_admin_task_filters_open_edit_item($form_state);
  }
}

function project_forecast_admin_task_filters_open_add_select(&$form_state) {
  $form = array();

  $view_settings = _project_forecast_tasks_view();
  $view = $view_settings['view'];
  $display_id = $view_settings['display_id'];

  module_load_include('inc', 'views', 'includes/admin');

  if (!empty($view)) {
    $base_tables = $view->get_base_tables();
    $options = views_fetch_fields(array_keys($base_tables), 'filter');
  }
  if (!empty($options)) {
    $groups = array('all' => t('<All>'));

    $form['name'] = array(
      '#prefix' => '<div class="views-radio-box form-checkboxes">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#default_value' => 'all',
    );

    foreach ($options as $key => $option) {
      $group = strtr(strtolower($option['group']), '_ ', '--');
      $groups[$group] = $option['group'];
      $form['name'][$key] = array(
        '#prefix' => '<div class="views-dependent-all views-dependent-'. $group .'">',
        '#suffix' => '</div>',
        '#type' => 'checkbox',
        '#title' => t('!group: !field', array('!group' => $option['group'], '!field' => $option['title'])),
        '#description' => $option['help'],
        '#return_value' => $key,
      );
    }
    $form['group']['#options'] = $groups;

    $form['buttons'] = array(
      '#prefix' => '<div class="clear-block"><div class="form-buttons">',
      '#suffix' => '</div></div>',
    );
    // remove default validate handler
    $form['#validate'] = array();

    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add filter'),
      '#submit' => array('project_forecast_admin_task_filters_open_add_submit'),
    );
    $form['buttons']['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#submit' => array('project_forecast_admin_task_filters_cancel'),
      '#validate' => array(),
    );
  }
  else {
    $form['markup'] = array(
      '#value' => '<div class="form-item">'. t('There are no filters available to add.') .'</div>',
    );
  }
  views_add_css('views-admin');

  $form_state['storage']['filters'] = variable_get('project_forecast_task_filters_open', array());
  return $form;
}

/**
 * Submit handler for adding new item(s) to a view.
 */
function project_forecast_admin_task_filters_open_add_submit($form, &$form_state) {
  if (!empty($form_state['values']['name']) && is_array($form_state['values']['name'])) {
    // Loop through each of the items that were checked and add them to the view.
    foreach (array_keys(array_filter($form_state['values']['name'])) as $field) {
      list($table, $field) = explode('.', $field, 2);

      // Check to see if this type has settings, if so add the settings form first.
      $handler = views_get_handler($table, $field, 'filter');
      $form_state['storage']['phase'] = ($handler && $handler->has_extra_options())
                                        ? 'config-item-extra'
                                        : 'config-item';
      $form_state['storage']['filter'] = array(
        'table' => $table,
        'field' => $field,
      );
    }
  }
  $form_state['rebuild'] = TRUE;
}

function project_forecast_admin_task_filters_open_edit(&$form_state, $key) {
  $form = array();
  $filters = variable_get('project_forecast_task_filters_open', array());

  if (isset($filters[$key])) {
    $form_state['storage']['filters'] = $filters;
    $form_state['storage']['filter_key'] = $key;
    $form_state['storage']['filter'] = $filters[$key];
    return project_forecast_admin_task_filters_open_edit_item($form_state);
  }
  else {
    $form['error'] = array(
      '#type' => 'markup',
      '#value' => t('The specified filter does not exist.'),
    );
  }
  return $form;
}

function project_forecast_admin_task_filters_open_edit_item(&$form_state) {
  $view_settings = _project_forecast_tasks_view();
  $view = $view_settings['view'];
  $display_id = $view_settings['display_id'];

  $filter = $form_state['storage']['filter'];

  $form = array('options' => array('#tree' => TRUE));
  if (!$view->set_display($display_id)) {
    views_ajax_render(t('Invalid display id @display', array('@display' => $display_id)));
  }
  $id = $view->add_item($display_id, 'filter', $filter['table'], $filter['field']);

  if (isset($form_state['storage']['filter_key']) && !empty($filter['item'])) {
    $view->set_item($display_id, 'filter', $id, $filter['item']);
  }
  $item = $view->get_item($display_id, 'filter', $id);

  if ($item) {
    $handler = views_get_handler($filter['table'], $filter['field'], 'filter');
    if (empty($handler)) {
      $form['markup'] = array('#value' => t("Error: handler for @table > @field doesn't exist!", array('@table' => $filter['table'], '@field' => $filter['field'])));
    }
    else {
      $handler->init($view, $item);
      $types = views_object_types();

      // A whole bunch of code to figure out what relationships are valid for
      // this item.
      $relationships = $view->display_handler->get_option('relationships');
      $relationship_options = array();

      foreach ($relationships as $relationship) {
        // relationships can't link back to self. But also, due to ordering,
        // relationships can only link to prior relationships.
        if ($type == 'relationship' && $id == $relationship['id']) {
          break;
        }
        $relationship_handler = views_get_handler($relationship['table'], $relationship['field'], 'relationship');
        // ignore invalid/broken relationships.
        if (empty($relationship_handler)) {
          continue;
        }

        // If this relationship is valid for this type, add it to the list.
        $data = views_fetch_data($relationship['table']);
        $base = $data[$relationship['field']]['relationship']['base'];
        $base_fields = views_fetch_fields($base, 'filter');
        if (isset($base_fields[$handler->table .'.'. $handler->field])) {
          $relationship_handler->init($view, $relationship);
          $relationship_options[$relationship['id']] = $relationship_handler->label();
        }
      }

      if (!empty($relationship_options)) {
        // Make sure the existing relationship is even valid. If not, force
        // it to none.
        $base_fields = views_fetch_fields($view->base_table, 'filter');
        if (isset($base_fields[$handler->table . '.' . $handler->field])) {
          $relationship_options = array_merge(array('none' => t('Do not use a relationship')), $relationship_options);
        }
        $rel = empty($item['relationship']) ? 'none' : $item['relationship'];
        if (empty($relationship_options[$rel])) {
          $rel = 'none';
        }

        $form['options']['relationship'] = array(
          '#type' => 'select',
          '#title' => t('Relationship'),
          '#options' => $relationship_options,
          '#default_value' => $rel,
        );
      }
      else {
        $form['options']['relationship'] = array(
          '#type' => 'value',
          '#value' => 'none',
        );
      }

      // Get form from the handler.
      $handler->options_form($form['options'], $form_state);

      // We don't want the expose button, doesn't make sense in this context.
      unset($form['options']['expose_button']);
      unset($form['options']['expose']);

      $form_state['storage']['handler'] = serialize($handler);

      $form['buttons']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#submit' => array('project_forecast_admin_task_filters_open_edit_submit'),
        '#validate' => array('project_forecast_admin_task_filters_open_edit_validate'),
      );
      $form['buttons']['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel'),
        '#submit' => array('project_forecast_admin_task_filters_cancel'),
        '#validate' => array(),
      );
    }
    drupal_set_title(t('!title - @filtername', array(
      '!title' => drupal_get_title(),
      '@filtername' => $handler->ui_name(),
    )));
  }
  views_add_css('views-admin');

  return $form;
}

function project_forecast_admin_task_filters_open_edit_validate($form, &$form_state) {
  // Make sure the class definition of the serialized handler is loaded
  // before unserializing it, otherwise we'll experience a nice fatal error.
  // The safest way to ensure that is to let Views load the handler itself.
  $filter = $form_state['storage']['filter'];
  $handler = views_get_handler($filter['table'], $filter['field'], 'filter');

  // We'd rather have our stateful handler object from before, though.
  $handler = unserialize($form_state['storage']['handler']);
  $handler->options_validate($form['options'], $form_state);
  $form_state['handler'] = $handler;
}

function project_forecast_admin_task_filters_open_edit_submit($form, &$form_state) {
  // Run it through the handler's submit function.
  $form_state['handler']->options_submit($form['options'], $form_state);
  $item = $form_state['handler']->options;
  $filter = $form_state['storage']['filter'];
  $filters = $form_state['storage']['filters'];
  $filter_key = $form_state['storage']['filter_key'];

  // Store the data we're given.
  foreach ($form_state['values']['options'] as $key => $value) {
    $item[$key] = $value;
  }
  $filter['item'] = $item;

  if (isset($filter_key) && isset($filters[$filter_key])) {
    $filters[$filter_key] = $filter; // overwrite existing filter
  }
  else {
    $filters[] = $filter; // new filter, add the item instead of replacing one
  }
  variable_set('project_forecast_task_filters_open', $filters);
  project_forecast_recalculate_all();
  drupal_goto('admin/project/forecast', 'show-view-settings=true');
}

function project_forecast_admin_task_filters_cancel(&$form_state) {
  drupal_goto('admin/project/forecast', 'show-view-settings=true');
}

/**
 * Menu callback for deleting an additional filter for the "open tasks" view.
 */
function project_forecast_admin_task_filters_open_delete($key) {
  $filters = variable_get('project_forecast_task_filters_open', array());
  if (isset($filters[$key])) {
    unset($filters[$key]);
  }
  variable_set('project_forecast_task_filters_open', $filters);
  project_forecast_recalculate_all();
  drupal_goto('admin/project/forecast', 'show-view-settings=true');
}
