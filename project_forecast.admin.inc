<?php
/**
 * @file
 * Project Forecast - Estimate completion dates for your project's tasks.
 *
 * This file contains the general parts of the administrative user interface.
 * The "Task filters" page has been outsourced to the .admin.filters.inc file
 * because of its complexity.
 *
 * Copyright 2007 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Form callback for "admin/project/forecast[/relationships]":
 * Let the admin select views of issues (plus arguments)
 * and other node relationships.
 */
function project_forecast_admin(&$form_state) {
  $view_options = array();
  $all_views = views_get_all_views();
  foreach ($all_views as $view) {
    $view_options[$view->name] = $view->name;
  }
  $tasks_view_name = variable_get('project_forecast_tasks_view', '');
  $filters = variable_get('project_forecast_task_filters_open', array());

  $form_state['original_tasks_view'] = $tasks_view_name;

  // Notify the user about missing additional filters, as it's easy to miss.
  if (empty($tasks_view_name)) {
    $filter_options[] = l(t('Please select a view before adding filters here.'));
  }
  else {
    include_once(drupal_get_path('module', 'project_forecast') . '/project_forecast.admin.filters.inc');

    $view_settings = _project_forecast_tasks_view();
    $tasks_view = $view_settings['view'];
    $display_id = $view_settings['display_id'];
    $tasks_view->set_display($display_id);
    $display = &$tasks_view->display[$display_id];

    // Get labels of the selected filters
    $filter_options = array();
    foreach ($filters as $key => $filter) {
      $handler = views_get_handler($filter['table'], $filter['field'], 'filter');
      if (empty($handler)) {
        continue;
      }
      $handler->init($tasks_view, $filter['item']);
      $filter_options[] = t('!filter-type @filter-summary (!delete)', array(
        '!filter-type' => l($handler->ui_name(),
          'admin/project/forecast/task-filters-open/edit/'. $key
        ),
        '@filter-summary' => $handler->admin_summary(),
        '!delete' => l(t('delete'), 'admin/project/forecast/task-filters-open/delete/'. $key),
      ));
    }

    if (empty($filters)) {
      $filter_options = '<div class="messages warning"><ul><li><strong>' . l(t('Add filter...'), 'admin/project/forecast/task-filters-open/add') . '</strong><br/>' . t('You have not configured any additional filters for reducing this view to only open tasks. Please add one or more such filters, so that Project Forecast can restrict its queries to just the tasks that need to be considered for target date estimate calculations.') . '</li></ul></div>';
    }
    else {
      $filter_options[] = l(t('Add filter...'), 'admin/project/forecast/task-filters-open/add');
      $filter_options = '<ul><li>' . implode('</li><li>', $filter_options) . '</li></ul>';
    }
  }

  $hours_providers = value_provider_options('hours');
  $nid_providers = value_provider_options('nid');
  $uid_providers = value_provider_options('uid');

  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('Task retrieval'),
    '#description' => t('Define how the list of tasks to be analyzed (...or issues, cases, bugs) is going be retrieved.'),
    '#collapsible' => TRUE,
    '#collapsed' => ($tasks_view_name != '' && !empty($filters) && empty($_GET['show-view-settings'])),
  );
  $form['general']['project_forecast_tasks_view'] = array(
    '#type' => 'select',
    '#title' => t('View of all tasks'),
    '#options' => array_merge(array('' => t('please select a view')), $view_options),
    '#required' => TRUE,
    '#default_value' => $tasks_view_name,
    '#description' => t('Choose the "Views module" view that selects all task nodes. The forecast will automatically filter this view according to its needs, using the other information that you provide here. For open tasks, the view\'s "sort criteria" section will be used to determine in which order the users are expected to complete these tasks.'),
  );
  $form['general']['project_forecast_tasks_view_args'] = array(
    '#type' => 'textfield',
    '#title' => t('View arguments'),
    '#default_value' => variable_get('project_forecast_tasks_view_args', ''),
    '#required' => FALSE,
    '#description' => t('Provide a comma separated list of arguments to pass to the above view.'),
  );
  $form['general']['project_forecast_tasks_view_filters_open'] = array(
    '#type' => 'item',
    '#title' => t('Filter(s) for only including open tasks'),
    '#value' => $filter_options,
  );
  $form['properties'] = array(
    '#type' => 'fieldset',
    '#title' => t('Task properties'),
    '#description' => t('Define which data from the task nodes is going to be used by the forecast.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['properties']['project_forecast_timeneed_provider'] = array(
    '#type' => 'select',
    '#title' => t('Field for the task\'s time need'),
    '#options' => empty($hours_providers)
                  ? array('' => t('Please create a duration field.'))
                  : $hours_providers,
    '#required' => TRUE,
    '#default_value' => variable_get('project_forecast_timeneed_provider', ''),
    '#description' => t('The project forecast needs to know how many time is still required for a given task, and this information is extracted from the field that you need to specify here. Tasks without this field are assumed to have a zero-length time need.'),
  );
  $form['properties']['project_forecast_project_nid_provider'] = array(
    '#type' => 'select',
    '#title' => t('Field for the project node that contains the task'),
    '#options' => empty($nid_providers)
                  ? array('' => t('Please create a node reference field.'))
                  : $nid_providers,
    '#required' => TRUE,
    '#default_value' => variable_get('project_forecast_project_nid_provider', ''),
    '#description' => t('The project forecast needs to know which project contains the given task, and this information is extracted from the field that you need to specify here.'),
  );
  $form['properties']['project_forecast_assigned_uid_provider'] = array(
    '#type' => 'select',
    '#title' => t('Field for the user who is assigned to this task'),
    '#options' => empty($uid_providers)
                  ? array('' => t('Please create a user reference field.'))
                  : $uid_providers,
    '#required' => TRUE,
    '#default_value' => variable_get('project_forecast_assigned_uid_provider', ''),
    '#description' => t('The project forecast needs to know which user a task is assigned to, in order to process the tasks separately for each user. This field is required to provide this information.'),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'project_forecast_admin_submit';

  return $form;
}

function project_forecast_admin_submit($form, &$form_state) {
  if ($form_state['values']['project_forecast_tasks_view'] != $form_state['original_tasks_view']) {
    // New view, new filters - the old ones most probably don't fit.
    variable_del('project_forecast_task_filters_open');
  }
  project_forecast_recalculate_all();
}
